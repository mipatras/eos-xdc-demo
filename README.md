## EOS-XDC Demo

This repository provides instructions on installing, deploying and running
EOS-XDC.  
Additional material (such as config or repo files) are provided as well.

The instructions here have been tested on a new CentOS 7 docker instance.


### Features and scenarios

The features brought by XDC allow EOS to absorb external storage resources
and operate them uniformly, as part of the same system.
This guide will explain how external storage resources can be
added to a running EOS instance, including importing existing files
into the namespace. From that moment on, the files can be managed in the usual
EOS way.

The following scenarios will be performed:

1. Importing existing files on the external storage into the EOS namespace
  - Replicating those files to another filesystem
  - Dropping the files located on the external storage
2. Uploading files to EOS, which will end up in the external storage
  - Replicating those files to another filesystem


### Description

EOS is a distributed storage system with a centralized namespace architecture.

This guide will deploy one namespace node (called MGM) and one storage node
(called FST). A message queue (MQ) node must also be deployed
to allow communication between all the nodes.

The nodes will be deployed on the same machine.


### Dependencies

XRootD 4.8.x is recommended for the EOS-XDC modifications.   
Although 4.9 versions should work as well, this demo will use 4.8.5 as that's
the latest stable version of XRootD at the moment.

The `repo/xrootd.repo` file provides the repository for XRootD:    
```
cp repo/xrootd.repo /etc/yum.repos.d/
```

Another prerequisite is being able to install [Davix][1].
This is provided by the epel-release:  
```
yum install -y epel-release`
```

Also, the EOS-XDC repo (provided by `repo/eos-el7-xdc.repo`) must be used.  
This installs the EOS-XDC and EOS-dependencies repositories:
```
cp repo/eos-el7-xdc.repo /etc/yum.repos.d/
```

The rest of the dependencies should be handled automatically by yum during
the installation process.


### Installing

The relevant packages for this demo are `eos-client` and `eos-server`:
```
yum install eos-client eos-server
```


### Configuration

Configuration of an EOS server comes in two flavors: XRootD related configuration
and EOS specific configuration.

For the XRootD configuration, a file is provided for each node:
```
[MGM]: config/xrd.cf.mgm
[MQ]:  config/xrd.cf.mq
[FST]: config.xrd.cf.fst
```

These files should be copied to `/etc/`:
```
cp config/xrd.cf.* /etc/
```

The EOS specific configuration provided here `config/eos_env`
will be common across all server roles and should be copied to
`/etc/sysconfig/`:
```
cp config/eos_env /etc/sysconfig/
```

The section below will detail only the parameters of interest to the demo.

#### xrd.cf files

Within the `xrd.cf.*` files, replace any `<mq_hostname>` occurence with the hostname of the MQ node.
In this example, they coincide with the local machine's hostname:

```
sed -i "<mq_hostname>/${HOSTNAME}/g" config/xrd.cf.*
```

#### EOS env variables

For the EOS environment variables, first of all, replace all occurences of `<mgm_hostname>` with the hostname of the MGM node.:
Again, in this demo, they coincide with the running machine's hostname:
```
sed -i "<mgm_hostname>/${HOSTNAME}/g" config/eos_env
```

Another very important variable is the path to the x509 certificate:

`EOS_FST_HTTPS_X509_CERTIFICATE_PATH="/path/to/x509/certificate"`

This should be enabled on the FST machine responsible for running the filesystem mapped to the WebDAV external storage resource.
Without a valid certificate, communication will not be possible.


### Running and using EOS

#### EOS server

To start an EOS server, using systemd is encouraged, via
the following command `systemd start eos@[role]`:
```
systemd start eos@mgm eos@mq eos@fst
```

#### EOS client

To use the client, simply use the `eos` executable. By default, it will try
to connect to `localhost`. To point the client to a different endpoint,
set the following environment variable `EOS_MGM_URL`:
```
export EOS_MGM_URL=root://<server_hostname>:1094/
```


### Setup

#### Adding filesystems

External storage endpoints are mapped within EOS to a filesystem.
These filesystems are added in the usual way and the system will map them
according to their URL protocol.  

##### Local filesystem prerequisites

To add a local filesystem, certain prerequisites have to be met.
The directory has to be owned and writable by the `daemon` user
(the user under which the xrootd process runs).
Also, EOS requires two hidden files to be found on the root of the filesystem,
namely:
- `.eosfsid` - contains the ID of the filesystem registered in EOS
- `.eosfsuuid` - contains the UUID of the filesystem registered in EOS

##### Remote filesystem prerequisites

A remote filesystem requires, first of all, that EOS have access to it.
The system also needs knowledge about the amount of space provided.
When dealing with a WebDAV endpoint, this information should be located
in a `.dav.quota` file.

##### Example

In our example, we will map a file system to the FST's local hard drive,
under `/data` and another one to DESY-hosted external storage,
having the following URL as the filesystem root:
`https://dcache-xdc.desy.de/Users/mipatras/`  
In order to have full control over how the files get placed,
we will assign the filesystems to different scheduling spaces:
`default` and `dcache`.

Note: a quota file is provided in `quota/dav.quota`. This should be uploaded
to `https://dcache-xdc.desy.de/Users/mipatras/.dav.quota`

Below are expressed the commands to setup and register the two filesystems
(the quota file is assumed to be in place):
```
# Local filesystem setup
mkdir /data
UUID=`uuidgen`
echo 1 > /data/.eosfsid
echo $UUID > /data/.eosfsuuid
chown daemon:daemon -R /data
chmod 755 /data

# Local filesystem
eos fs add -m 1 $UUID <fst_hostname>:1095 /data default rw

# External storage filesystem
eos fs add -m 2 `uuidgen` <fst_hostname>:1095 https://dcache-xdc.desy.de/Users/mipatras/ dcache rw

# View the filesystems
eos fs ls
```

#### Preparing EOS directories

In our example, we will work with two directories,
each of them assigned to a different scheduling space:
- `/eos/xdc/local` - assigned to the `default` scheduling space
- `/eos/xdc/dcache` - assigned to the `dcache` scheduling space

The commands below will create the 2 directories and assign them
to their respective scheduling space:
```
# Directory for local filesystem
eos mkdir /eos/xdc/local
eos attr set sys.forced.space=default /eos/xdc/local

# Directory for external storage filesystem
eos mkdir /eos/xdc/dcache
eos attr set sys.forced.space=dcache /eos/xdc/dcache
```


### Performing the scenarios

I. Importing existing files on the external storage into the EOS namespace

Initial Setup:
-  1000 files of 1MB size have been uploaded to
`https://dcache-xdc.desy.de/Users/mipatras/demo/`,   
with  the following naming convention: *file#.1mb*
- Responsible FST is assumed to have a valid certificate for RW operations
on the dCache endpoint

Files from `https://dcache-xdc.desy.de/Users/mipatras/demo/` will be imported
into `/eos/xdc/dcache/`.  
After following the **Setup > Preparing EOS directories** section, the import
operation is ready:
```
eos fs import start 2 https://dcache-xdc.desy.de/Users/mipatras/demo/ /eos/xdc/dcache/import/

# To query operation status
# eos fs import query <import_uuid>

# Check the files are there
eos ls -l /eos/xdc/dcache/import/
eos fileinfo /eos/xdc/dcache/file1000.mb --fullpath

# Replicate files
for i in `seq 1 1000` ; do eos file replicate /eos/xdc/dcache/import/file${i}.1mb 2 1 ; done
eos fileinfo /eos/xdc/dcache/file1000.mb --fullpath

# Drop remote replica
for i in `seq 1 1000` ; do eos file drop /eos/xdc/dcache/import/file${i}.1mb 2 ; done
eos fileinfo /eos/xdc/dcache/file1000.mb --fullpath
```

II. Uploading files to EOS, which will end up in the external storage

For this scenario, upon following the **Setup > Preparing EOS directories**
section, simply upload files as with any other EOS directory.

```
# Create test file
dd if=/dev/urandom of=file32mb.demo bs=1MB count=32

eos mkdir /eos/xdc/dcache/upload/
xrdcp file32mb.demo root://<mgm_hostname>:1094//eos/xdc/dcache/upload/file32mb.demo

# Check file arrived
eos fileinfo /eos/xdc/dcache/upload/file32mb.demo --fullpath

# Delete file
eos rm /eos/xdc/dcache/upload/file32mb.demo

# After 30 seconds (the FST delete scheduling time)
# check file disappeared from dCache endpoint
davix-ls http://dcache-xdc.desy.de/Users/mipatras/eos/xdc/dcache/upload/file32mb.demo
```

[1]: https://dmc.web.cern.ch/projects/davix/home
